import styled from "styled-components";

export const Title = styled.h1`
  padding: 0.3rem;
  text-align: center;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 5rem;
`;
