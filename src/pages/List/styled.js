import styled from "styled-components";
import MateriaButton from "@mui/material/Button";

export const ListContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const ListItem = styled.li`
  height: 3rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 90%;
`;

export const Button = styled(MateriaButton)``;
