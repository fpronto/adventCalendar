import { useState, useEffect } from "react";
import dayjs from "dayjs";

import { ListContainer, Button } from "./styled";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";
import { useSnackbar } from "notistack";

import { useClient } from "../../hooks/useSupabaseClient";

/**
 * Generate a random number between a minimum and maximum value.
 *
 * @param {number} min - the minimum value (inclusive)
 * @param {number} max - the maximum value (inclusive)
 * @return {number} a random number between min and max
 */
function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Finds the most frequently occurring item based on a given key in an array.
 *
 * @param {Array} array - The array to search for the most used item.
 * @param {string} key - The key to use for comparison.
 * @return {string|undefined} - The most frequently occurring item based on the key, or undefined if the array is empty.
 */
function getMostUsedItemByKey(array, key) {
  if (array.length === 0) {
    return undefined;
  }

  const frequencyMap = {};
  let maxCount = 0;
  let mostUsedItem;

  for (const item of array) {
    frequencyMap[item[key]] = (frequencyMap[item[key]] || 0) + 1;

    if (frequencyMap[item[key]] > maxCount) {
      maxCount = frequencyMap[item[key]];
      mostUsedItem = item[key];
    } else if (frequencyMap[item[key]] === maxCount) {
      mostUsedItem = undefined;
    }
  }

  return mostUsedItem;
}

const List = () => {
  const { enqueueSnackbar } = useSnackbar();

  const supabaseClient = useClient();
  const [open, setOpen] = useState(false);
  const [currentList, setCurrenList] = useState([]);
  const [dayTask, setDayTask] = useState(null);

  useEffect(() => {
    const fetchInitiaData = async () => {
      const { data: daysList, error } = await supabaseClient
        .from("List")
        .select("*")
        .neq("dayUsed", "");
      setCurrenList(
        daysList.sort((a, b) => Number(a?.dayUsed) - Number(b?.dayUsed))
      );
    };
    fetchInitiaData();
  }, [supabaseClient]);

  const handleOnNextDay = (isIndoor) => async () => {
    const { data: List, error: gettingError } = await supabaseClient
      .from("List")
      .select("*")
      .eq("dayUsed", "")
      .eq("indoor", isIndoor);
    const usernameWithMost = getMostUsedItemByKey(currentList, "username");
    if (List.length === 0) {
      enqueueSnackbar(
        `Todos os itens ${isIndoor ? "dentro" : "fora"} de casa foram usados!`,
        { variant: "error" }
      );
      closeConfirm();
      return;
    }
    const randomNumber = getRandomNumber(0, List.length - 1);
    let listToPick = List;
    if (usernameWithMost) {
      listToPick = List.filter((item) => item.username !== usernameWithMost);
    }
    const itemToUse = listToPick[randomNumber];
    setDayTask(itemToUse);

    itemToUse.dayUsed = dayjs().get("date").toString();
    const { data, error } = await supabaseClient
      .from("List")
      .update({ dayUsed: dayjs().get("date") })
      .eq("id", itemToUse.id)
      .select();
    setCurrenList([...currentList, itemToUse]);
    closeConfirm();
  };
  const openDialog = () => {
    const todayUsed = currentList.find(
      (item) => item?.dayUsed === dayjs().get("date").toString()
    );
    const notDecember = dayjs().get("month") !== 11;
    if (notDecember) {
      enqueueSnackbar("Dezembro ainda não chegou...", { variant: "error" });
      return;
    }
    if (todayUsed) {
      enqueueSnackbar("Já existe uma tarefa para o dia", { variant: "error" });
      return;
    }
    setOpen(true);
  };
  const closeConfirm = () => {
    setOpen(false);
  };
  return (
    <ListContainer>
      <Dialog
        sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
        maxWidth="xs"
        open={open}
      >
        <DialogTitle>Confirmação</DialogTitle>
        <DialogContent dividers>Dentro de Casa</DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleOnNextDay(false)}>
            Não
          </Button>
          <Button onClick={handleOnNextDay(true)}>Sim</Button>
        </DialogActions>
      </Dialog>
      <Dialog
        sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
        maxWidth="xs"
        open={Boolean(dayTask)}
      >
        <DialogTitle>Tarefa do dia</DialogTitle>
        <DialogContent dividers>{dayTask?.todo}</DialogContent>
        <DialogActions>
          <Button onClick={() => setDayTask(null)}>Ok</Button>
        </DialogActions>
      </Dialog>
      <Button variant="contained" onClick={openDialog}>
        Tirar próximo dia
      </Button>
      <div>
        {currentList.map((item) => (
          <div key={item?.id}>
            {item?.dayUsed} - {item?.todo}
          </div>
        ))}
      </div>
    </ListContainer>
  );
};

export default List;
