import styled from "styled-components";

import MaterialButton from "@mui/material/Button";
import MaterialIconButton from "@mui/material/IconButton";
import MaterialTextField from "@mui/material/TextField";

export const Title = styled.h1`
  padding: 0.3rem;
  text-align: center;
`;

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const ListItem = styled.li`
  height: 5rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 90%;
`;

export const IconButton = styled(MaterialIconButton)`
  color: white !important;
`;

export const Button = styled(MaterialButton)`
  margin-left: 0.5rem !important;
`;

export const BigButton = styled(MaterialButton)`
  width: 80%;
  background-color: ${(props) => props.$color} !important;
  margin: 1rem 0 !important;
`;

export const TextField = styled(MaterialTextField)`
  margin: 0.5rem !important;
  & * {
    color: white !important;
    border-color: white !important;
  }
`;
