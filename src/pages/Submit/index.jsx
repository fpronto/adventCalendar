import { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import SaveIcon from "@mui/icons-material/Save";
import EditIcon from "@mui/icons-material/Edit";
import FormControlLabel from "@mui/material/FormControlLabel";
import NoMeetingRoomIcon from "@mui/icons-material/NoMeetingRoom";
import MeetingRoomIcon from "@mui/icons-material/MeetingRoom";
import Checkbox from "@mui/material/Checkbox";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";

import { useSnackbar } from "notistack";

import { v4 } from "uuid";
import { useClient } from "../../hooks/useSupabaseClient";

import {
  MainContainer,
  ListItem,
  Button,
  BigButton,
  List,
  IconButton,
  TextField,
} from "./styled.js";

const Submit = () => {
  const { enqueueSnackbar } = useSnackbar();
  const supabaseClient = useClient();
  const [username, setUsername] = useState("");
  const [open, setOpen] = useState(false);
  const [list, setList] = useState(
    Array.from({ length: 12 }, (_, i) => ({
      key: v4(),
      text: `Item ${i + 1}`,
      indoor: false,
    }))
  );
  const [edit, setEdit] = useState();
  const [editingValue, setEditingValue] = useState("");
  const [isInDoor, setIsInDoor] = useState(false);
  const handleOnPlus = () => {
    setList([
      ...list,
      { key: v4(), text: `Item ${list.length + 1}`, indoor: false },
    ]);
  };
  const handleOnChange = (field) => (e) => {
    switch (field) {
      case "username":
        setUsername(e.target.value);
        break;
      case "editingValue":
        setEditingValue(e.target.value);
        break;
      default:
        break;
    }
  };

  const handleOnSubmit = async () => {
    closeConfirm();
    const insertingItems = list.map((item) => ({
      todo: item.text,
      indoor: Boolean(item.indoor),
      username,
      dayUsed: "",
    }));
    const { data, error } = await supabaseClient
      .from("List")
      .insert(insertingItems)
      .select();

    if (!error) {
      enqueueSnackbar(`Foram enviados ${data.length} novos items`, {
        variant: "success",
      });
    }
  };
  const handleOnSaveEditable = () => {
    setList(
      list.map((item, index) =>
        index === edit
          ? { key: item.key, text: editingValue, indoor: isInDoor }
          : item
      )
    );
    setEdit();
  };
  const handleOnDelete = (index) => () => {
    setList(list.filter((item, i) => i !== index));
  };
  const openConfirm = () => {
    if (!username) {
      enqueueSnackbar("Falta o nome no campo username", { variant: "error" });
      return;
    }
    if (list.some((item) => item.text === "")) {
      enqueueSnackbar("Alguns itens na lista estão vazios", {
        variant: "error",
      });
      return;
    }

    setOpen(true);
  };
  const closeConfirm = () => {
    setOpen(false);
  };

  return (
    <MainContainer>
      <Dialog
        sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
        maxWidth="xs"
        open={open}
      >
        <DialogTitle>Confirmação</DialogTitle>
        <DialogContent dividers>
          Tens a certeza que queres enviar? Não é possivel voltar atrás!
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={closeConfirm}>
            Cancelar
          </Button>
          <Button onClick={handleOnSubmit}>Ok</Button>
        </DialogActions>
      </Dialog>
      {list.length < 12 ? (
        <BigButton
          variant="contained"
          $color={"#0038ff"}
          onClick={handleOnPlus}
        >
          Mais
        </BigButton>
      ) : (
        <BigButton variant="contained" $color={"green"} onClick={openConfirm}>
          Submeter
        </BigButton>
      )}
      <TextField
        onChange={handleOnChange("username")}
        label={`Username`}
        variant="outlined"
        value={username}
      />
      <List>
        {list.map((item, index) => {
          if (edit === index) {
            return (
              <ListItem key={item.key}>
                <TextField
                  onChange={handleOnChange("editingValue")}
                  label={`Item ${index + 1}`}
                  variant="outlined"
                  value={editingValue}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      checked={isInDoor}
                      onChange={() => setIsInDoor(!isInDoor)}
                      aria-label="login"
                    />
                  }
                  label="Em Casa"
                />
                <IconButton onClick={handleOnSaveEditable}>
                  <SaveIcon />
                </IconButton>
              </ListItem>
            );
          }
          return (
            <ListItem key={item.key}>
              {index + 1}. {item.text}
              {item.indoor ? <MeetingRoomIcon /> : <NoMeetingRoomIcon />}
              <div>
                <IconButton
                  disabled={edit !== undefined}
                  onClick={() => {
                    setEdit(index);
                    setEditingValue(item.text);
                    setIsInDoor(item.indoor);
                  }}
                >
                  <EditIcon />
                </IconButton>
                <IconButton
                  disabled={edit !== undefined}
                  onClick={handleOnDelete(index)}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
            </ListItem>
          );
        })}
      </List>
    </MainContainer>
  );
};

export default Submit;
