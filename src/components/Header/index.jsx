import { useState } from "react";
import { Link } from "react-router-dom";
import { Title, MenuList, HeaderContainer, MenuIcon } from "./styled.js";
import IconButton from "@mui/material/IconButton";

const Header = () => {
  const [showing, setShowing] = useState(false);
  return (
    <HeaderContainer>
      <Link to="/">
        <Title>Calendário do Advento</Title>
      </Link>
      <IconButton onClick={() => setShowing(!showing)}>
        <MenuIcon />
      </IconButton>
      <MenuList $show={showing}>
        <Link to="/submit" onClick={() => setShowing(false)}>
          Adicionar Mais
        </Link>
        <Link to="/list" onClick={() => setShowing(false)}>
          Lista
        </Link>
      </MenuList>
    </HeaderContainer>
  );
};

export default Header;
