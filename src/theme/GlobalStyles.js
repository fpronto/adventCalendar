import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

body {

  font-family: 'DM Sans', sans-serif;
  font-size:20px;
  color: ${(props) => props.theme.text};
  text-align: justify;
  text-justify: inter-word;
}

span {
  font-family: 'DM Serif Display', sans-serif;
  letter-spacing: 3px;
  font-size: 5rem;
  font-weight: bold;
}

h1, h2 {
  font-family: 'DM Serif Display', sans-serif;
  letter-spacing: 3px;
}

h3, h4, h5, h6, p {}

header, footer{
  background: ${(props) => props.theme.primary};
  color: ${(props) => props.theme.text};
  left: 0;
  right: 0;
  z-index: 999;
  display: flex;
  position: relative;
  justify-content: space-between;
}

ul, li {
  list-style: none;
}

button, a {
  cursor: pointer;
  transition: 1s;
  :hover {
    transition: 0.5s;
    color: ${(props) => props.theme.hover};
  }
}

* {
  margin: 0;
  padding: 0;  
  box-sizing: content-box;
  border:none;
  outline: none;
  text-decoration:none;
  color:inherit;
}

`;
