import { createContext } from "react";

const initialData = {
  client: null,
  session: null,
};
export const Context = createContext(initialData);
// eslint-disable-next-line react/prop-types
export function Provider({ value: client, children }) {
  return <Context.Provider value={{ client }}>{children}</Context.Provider>;
}
export const Consumer = Context.Consumer;
Context.displayName = "SupabaseContext";
