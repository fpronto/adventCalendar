import styled from "styled-components";

export const Container = styled.div`
  background-color: rgb(0 0 0 / 50%);
  filter: drop-shadow(0px 0px 50px black);
  width: 100%;
  height: 100%;
`;
