import { useContext } from "react";

import { Context } from "../context";

export function useClient() {
  const context = useContext(Context);
  if (context === undefined)
    throw Error("No client has been specified using Provider.");
  const { client } = context;
  return client;
}
