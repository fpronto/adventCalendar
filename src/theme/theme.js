import { createTheme } from "@mui/material/styles";
const theme = createTheme({
  primary: "#333333",
  secondary: "#CCCCCC",
  background: "#BBBBBB",
  text: "#FFFFFF",
  text_secondary: "#333333",
  hover: "#7D7D7D",
  palette: {
    mode: "dark",
  },
});

export default theme;
