import { Routes, Route, Navigate } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import { createClient } from "@supabase/supabase-js";
import { Provider } from "./context";
import { SnackbarProvider } from "notistack";
import { Container } from "./styled";
import GlobalStyles from "./theme/GlobalStyles";

import theme from "./theme/theme";

import Header from "./components/Header";
// import ProtectedRoute from "./components/ProtectedRoute";
// import HomePage from "./pages/Home";
import ListPage from "./pages/List";
import SubmitPage from "./pages/Submit";

const App = () => {
  const supabaseKey = import.meta.env.VITE_SUPABASE_KEY;
  const supabaseUrl = import.meta.env.VITE_SUPABASE_URL;
  const supabase = createClient(supabaseUrl, supabaseKey, {
    auth: {
      autoRefreshToken: true,
      persistSession: true,
      detectSessionInUrl: true,
    },
  });

  return (
    <Provider value={supabase}>
      <ThemeProvider theme={theme}>
        <SnackbarProvider>
          <GlobalStyles />
          <Container>
            <Header />
            <Routes>
              <Route exact path="/submit" element={<SubmitPage />} />
              <Route exact path="/list" element={<ListPage />} />
              <Route path="*" element={<Navigate to="/list" />} />
            </Routes>
          </Container>
        </SnackbarProvider>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
