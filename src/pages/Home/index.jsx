import { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Switch from "@mui/material/Switch";
import { Container } from "./styled";
import { useNavigate } from "react-router-dom";

import { useClient } from "../../hooks/useSupabaseClient";

const Home = () => {
  const navigate = useNavigate();
  const supabaseClient = useClient();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [loginUsername, setLoginUsername] = useState("");
  const [loginPassword, setLoginPassword] = useState("");

  const [isLogin, setIsLogin] = useState(false);

  const handleOnChange = (field) => (e) => {
    switch (field) {
      case "username":
        setUsername(e.target.value);
        break;
      case "password":
        setPassword(e.target.value);
        break;
      case "loginUsername":
        setLoginUsername(e.target.value);
        break;
      case "loginPassword":
        setLoginPassword(e.target.value);
        break;

      default:
        break;
    }
  };

  const handleOnSubmit = async () => {
    const { data, error } = await supabaseClient.auth.signUp({
      email: username,
      password,
    });
    console.log("data, error -> ", data, error);
    // debugger;
  };

  const handleOnLogin = async () => {
    const { data, error } = await supabaseClient.auth.signInWithPassword({
      email: loginUsername,
      password: loginPassword,
    });
    console.log("!error -> ", !error);
    if (!error) {
      navigate("/submit");
    }
  };

  return (
    <div>
      <p>Bem vindo ao nosso Calendário do Advento</p>
      <Switch onChange={() => setIsLogin(!isLogin)} aria-label="login" />

      {!isLogin ? (
        <Container>
          <TextField
            onChange={handleOnChange("username")}
            label="Username"
            variant="outlined"
            value={username}
          />
          <TextField
            onChange={handleOnChange("password")}
            label="Password"
            variant="outlined"
            type="password"
            value={password}
          />
          <Button onClick={handleOnSubmit} variant="contained" color="primary">
            Criar Conta
          </Button>
        </Container>
      ) : (
        <Container>
          <TextField
            onChange={handleOnChange("loginUsername")}
            label="Login Username"
            variant="outlined"
            value={loginUsername}
          />
          <TextField
            onChange={handleOnChange("loginPassword")}
            label="Login Password"
            variant="outlined"
            type="password"
            value={loginPassword}
          />
          <Button onClick={handleOnLogin} variant="contained" color="primary">
            Login
          </Button>
        </Container>
      )}
    </div>
  );
};

export default Home;
