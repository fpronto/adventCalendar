import styled from "styled-components";
import MateriaMenuIcon from "@mui/icons-material/Menu";

export const Title = styled.h1`
  padding: 0.3rem;
  text-align: center;
  filter: drop-shadow(0.4rem 0.4rem 1.65rem black);
`;

export const HeaderContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
`;

export const MenuList = styled.nav`
  display: ${(props) => (props.$show ? "flex" : "none")};
  position: absolute;
  top: 10rem;
  z-index: 5;
  background-color: black;
  align-items: center;
  justify-content: center;
  width: 100%;
  & * {
    margin: 1rem;
  }
`;

export const MenuIcon = styled(MateriaMenuIcon)`
  color: white;
`;
